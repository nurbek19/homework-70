import {ADD, CLEARVALUE, CLEARALL, GETVALUE} from "./action";

const initialState = {
    screenString: '',
    items: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, '+', '-', '*', '/', '.', '<', 'CE', '=']
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD:
            if (state.items.indexOf(state.screenString[state.screenString.length - 1]) >= 0 && isNaN(action.value)) {
                return {
                    ...state,
                    screenString: state.screenString.substring(0, state.screenString.length - 1) + action.value
                };
            } else {
                return {
                    ...state,
                    screenString: state.screenString + action.value
                };
            }
        case CLEARVALUE:
            const currentState = state.screenString;
            return {
                ...state,
                screenString: currentState.substring(0, currentState.length - 1)
            };
        case CLEARALL:
            return {...state, screenString: ''};
        case GETVALUE:
            let value;

            try {
                value = eval(state.screenString);
                return {...state, screenString: value};
            } catch (e) {
                alert('Something get wrong!');
                return {...state, screenString: ''};
            }
        default:
            return state;
    }
};

export default reducer;