export const ADD = 'ADD';
export const CLEARVALUE = 'CLEARVALUE';
export const CLEARALL = 'CLEARALL';
export const GETVALUE = 'GETVALUE';

export const addValue = value => {
  return {type: ADD, value}
};

export const clearValue = () => {
    return {type: CLEARVALUE}
};

export const clearAll = () => {
    return {type: CLEARALL}
};

export const getValue = () => {
    return {type: GETVALUE}
};
