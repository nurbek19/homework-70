import React, {Component, Fragment} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';

import CalculatorItem from '../../components/CalculatorItem/CalculatorItem';
import {addValue, clearValue, clearAll, getValue} from "../../store/action";

class Calculator extends Component {

    render() {
        return (
            <Fragment>
                <View style={styles.container}>
                    <View
                        style={{width: '100%', height: 100, borderWidth: 1, borderColor: 'blue', marginBottom: 15, justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{fontSize: 20}}>{this.props.screenString}</Text>
                    </View>
                    {this.props.items.map(item => {
                        if (item === '+' || item === '-' || item === '*' || item === '/') {
                            const value = item;

                            return <CalculatorItem key={item}
                                                   containerStyle={styles.narrowBlock}
                                                   addValue={() => this.props.addValue(value)}
                                                   blockStyle={styles.blueItem}
                                                   value={item}
                            />
                        }         else if (item === '<') {
                           return <CalculatorItem key={item}
                                                  containerStyle={styles.narrowBlock}
                                                  addValue={this.props.clearValue}
                                                  blockStyle={styles.greenItem}
                                                  value={item}
                           />
                       } else if (item === 'CE') {
                           return <CalculatorItem key={item}
                                                  containerStyle={styles.narrowBlock}
                                                  addValue={this.props.clearAll}
                                                  blockStyle={styles.greenItem}
                                                  value={item}
                           />
                       } else if (item === '=') {
                           return <CalculatorItem key={item}
                                                  containerStyle={styles.narrowBlock}
                                                  addValue={this.props.getValue}
                                                  blockStyle={styles.greenItem}
                                                  value={item}
                           />
                       } else if (item === '.') {
                            return <CalculatorItem key={item}
                                                   containerStyle={styles.narrowBlock}
                                                   addValue={() => this.props.addValue(item)}
                                                   blockStyle={styles.greenItem}
                                                   value={item}
                            />
                        } else if (item === 0) {
                            return <CalculatorItem key={item}
                                                   containerStyle={styles.expandBlock}
                                                   addValue={() => this.props.addValue(item)}
                                                   blockStyle={styles.greenItem}
                                                   value={item}
                            />
                        } else {
                            return <CalculatorItem key={item}
                                                   containerStyle={styles.wideBlock}
                                                   addValue={() => this.props.addValue(item)}
                                                   blockStyle={styles.greenItem}
                                                   value={item}
                            />
                        }

                    })}
                </View>
            </Fragment>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        paddingVertical: 30,
        paddingHorizontal: 20,
        backgroundColor: '#fff'
    },
    wideBlock: {width: '30%', marginBottom: 15},
    narrowBlock: {width: '22%', marginBottom: 15},
    expandBlock: {width: '100%', marginBottom: 15},
    greenItem: {backgroundColor: 'green', paddingVertical: 15, textAlign: 'center'},
    blueItem: {backgroundColor: 'blue', paddingVertical: 15, textAlign: 'center', color: '#fff'}
});


const mapStateToProps = state => {
    return {
        screenString: state.screenString,
        items: state.items
    }
};

const mapDispatchToProps = dispatch => {
    return {
        addValue: (value) => dispatch(addValue(value)),
        clearValue: () => dispatch(clearValue()),
        clearAll: () => dispatch(clearAll()),
        getValue: () => dispatch(getValue())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);