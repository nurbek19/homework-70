import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';

const CalculatorItem = props => {
    return (
        <TouchableOpacity style={props.containerStyle} onPress={props.addValue}>
            <Text style={props.blockStyle}>{props.value}</Text>
        </TouchableOpacity>
    )
};

export default CalculatorItem;